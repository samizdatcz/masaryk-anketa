$(document).ready(function() {
  $.getJSON( "data/data.json", function(data) {
    
    $(data).each(function(i) {
      $("<a class='open-popup-link' href='#popup-" + i + "'><div class='respondent'></div></a>").appendTo("#anketa");
      $(".respondent").last().append("<div class='cedulka'>Klikněte pro celou odpověď</strong></div>");
      $(".respondent").last().append("<img class='portret' src='https://samizdat.blob.core.windows.net/storage/anketa-masaryk/" + this.foto + "'>");
      $(".respondent").last().append("<p class='jmeno'><strong>" + this.jmeno + " " + this.prijmeni + "</strong>, " + this.medium + "</p>");
      $(".respondent").last().append("<div class='stars'>" + this.hvezdy + "</div>");
      $(".respondent").last().append("<p class='veta'>&bdquo;" + this.veta + "&ldquo;</p>");
      $(".respondent").last().append("<div class='white-popup mfp-hide' id='popup-" + i + "'><p><strong>" + this.jmeno + " " + this.prijmeni + "</strong>, " + this.medium + "</p><p><em>1. Souhlasíte s hodnocením poroty Českého lva, že si snímek Masaryk zaslouží ocenění hned ve 12 kategoriích?</em></p><p>" + this.odpoved1 + "</p><hr><p><em>2. Kolik pomyslných hvězdiček (nejvíc pět) byste filmu dal/a?</em></p><p><div class='stars'>" + this.hvezdy + "</div></p><hr><p><em>3. Jak byste v krátkosti snímek zhodnotil/a?</em></p><p>" + this.odpoved2 + "</p></div>");
    });

    $(".cedulka").hide();

    $(".respondent").hover(
      function() {
        $(this).addClass("vybrany");
        $(this).find(".cedulka").show();
      }, function() {
        $(this).removeClass("vybrany");
        $(this).find(".cedulka").hide();
      }
    );
    
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

  });
});