---
title: "12 Českých lvů pro Masaryka? Nesouhlasíme, hodnotí film oslovení kritici"
perex: "Drama Masaryk o životě syna prvního československého prezidenta ovládlo 24. ročník cen Český lev. Snímek Julia Ševčíka zvítězil ve většině kategorií. Česká filmová a televizní akademie ho ocenila mimo jiné za nejlepší film, režii, scénář a mužské herecké výkony v hlavní a vedlejší roli. Zpravodajský web Českého rozhlasu proto oslovil české filmové kritiky, aby odpověděli na otázku, zda je podle nich rekordní ocenění oprávněné."
description: "Zpravodajský web Českého rozhlasu oslovil české filmové kritiky, aby odpověděli na otázku, zda je podle nich rekordní počet Českých lvů pro film Masaryk na místě."
authors: ["Kristina Roháčková"]
published: "10. března 2017"
socialimg: http://media.rozhlas.cz/_obrazek/3804751--karel-roden-v-hlavni-roli-dramatu-masaryk--1-810x562p0.jpeg
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
# url: "brexit"
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
libraries: [jquery]
styles: ["styl/anketa.css", "styl/magnific-popup.css"]
recommended:
  - link: http://www.rozhlas.cz/zpravy/film/_zprava/filmova-akademie-kvuli-masarykovi-zvazi-zmenu-pravidel-ceskych-lvu--1704957
    title: Filmová akademie kvůli Masarykovi zváží změnu pravidel Českých lvů
    perex: Některým to přirozeně zkazilo náladu, to využití, zneužití pravidel. Musíme si říct, jestli je to opravdu zásadní zneužití, nebo producentská strategie, která si vyžádá úpravu stanov, řekl Mathé Radiožurnálu.
    image: http://media.rozhlas.cz/_obrazek/3321273--cesky-lev--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/film/_zprava/1704681
    title: Český lev na sítích: A vyhrává film, který svět neviděl
    perex: Nejlepším filmem roku 2016 se stal film Masaryk, který má širší premiéru v roce 2017.
    image: http://media.rozhlas.cz/_obrazek/3814416--jiri-bartoska-a-eva-zaoralova-clenove-sine-slavy--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/film/_zprava/ve-stinu-otce-historie-a-vlastnich-traumat-masaryk-nezklame-i-tak-ale-spise-klouze-po-povrchu--1703801
    title: RECENZE: Masaryk nezklame, i tak ale spíše klouže po povrchu
    perex: Masaryk jako diplomat, Masaryk jako šašek, Masaryk jako blázen. 
    image: http://media.rozhlas.cz/_obrazek/3812420--snimek-z-filmu-masaryk--1-620x349p0.jpeg
---

Ceny Český lev se vyhlašují každoročně. O vítězi v jednotlivých kategoriích rozhodují členové České filmové a televizní akademie v rámci neveřejného, dvoukolového hlasování. Do prvního kola hlasování se dostanou všechny snímky, které splňují podmínky Akademie. V druhém kole se pak rozhoduje mezi pěti filmy, které ve své kategorii dostaly nejvíce hlasů. Podle stránek Českých lvů hlasovalo ve druhém kole 157 členů. 
  
Členem Akademie se může stát držitel alespoň tří nominací, ceny Českého lva, některé z mezinárodních filmových cen (např. César, BAFTA, Zlatý glób nebo Goya) či festivalových ocenění nebo významná osobnost české či světové kinematografie.

<aside class="big">
  <h2 data-bso=1 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">1. Souhlasíte s hodnocením poroty Českého lva, že si snímek Masaryk zaslouží ocenění hned ve 12 kategoriích?</h2>
  <h2 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">2. Kolik pomyslných hvězdiček (nejvíc pět) byste filmu dal/a?</h2>
  <h2 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">3. Jak byste v krátkosti snímek zhodnotil/a?</h2>
  <h3 style="margin-left: 25px; margin-right: 25px; margin-bottom: 20px; margin-top: 30px; color: #606060">Kliknutím na respondenta zobrazíte jeho celé odpovědi</h3>
  <div id="anketa">
  </div>
</aside>

Masaryk na Lvech obhájil 12 ze 14 nominací. Jeho oficiální premiéra přitom následovala až týden po ceremoniálu, což vyvolalo vlnu kritiky v médiích i mezi lidmi na sociálních sítích. Prezident ČFTA Ivo Mathé dva dny poté [prohlásil, že je nutné zvážit případnou změnu pravidel](http://www.rozhlas.cz/zpravy/film/_zprava/1704957). 

K nařčení se o den později vyjádřil i ředitel českého distributora snímku společnosti Bioscop Robert Schaffer. „Limitované uvedení filmu na konci minulého roku bylo plánované a dopředu konzultované s Českou filmovou a televizní akademií, a to už na jaře 2016 formou oficiálního dotazu, na nějž vedení ČFTA odpovědělo kladně,“ [prohlásil Schaffer](http://www.rozhlas.cz/zpravy/film/_zprava/akademici-masaryka-svymi-prohlasenimi-poskozuji-o-nasazeni-do-kin-vedeli-brani-se-distributor-filmu--1705555).